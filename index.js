const langId = {
    "Фотографии" : "silenceItemItemPhoto",
    "Видеозаписи" : "silenceItemVideo",
    "Друзья" : "silenceItemFriends",
    "Сообщества" : "silenceItemGroups",
    "Подкасты" : "silenceItemPodkasts"
};

document.addEventListener("DOMContentLoaded", function(){

    const person = document.querySelector('.person');
    const topSilenceMenu = document.querySelector('.topSilenceMenu');
    const inputInContainer1 = document.querySelector('.container-1').querySelector('input');
    const searchSilenceMenu = document.querySelector('.searchSilenceMenu');
    const ringIcon = document.querySelector('.ring');
    const ringSilenceMenu = document.querySelector('.ringSilenceMenu');
    const musicIcon = document.querySelector('.music');
    const musicSilenceMenu = document.querySelector('.musicSilenceMenu');
    const silenceWalkman = document.querySelector('.silenceWalkman');
    const switchBtn = document.querySelector('.switch-btn');
    const newsPag = document.querySelectorAll('.newsPag');
    const newsPagItem = document.querySelector('.newsPagItem');
    const silenceBlock = document.querySelector('.silenceBlock');
    const newsPagItemIconPlus = newsPagItem.querySelector('.fa-plus');
    const newsSilenceMenu = document.querySelector('.newsSilenceMenu');
    const silenceNewsItem = document.querySelectorAll('.silenceNewsItem');
    const arrowLeft = document.querySelector('.historyItemsArrow.left');
    const arrowRight = document.querySelector('.historyItemsArrow.right');
    const preview = document.querySelector('.preview');
    const previewChildren = preview.children;
    const whatsNewBlock = document.querySelector('.whatsNew');
    const iconWrap = document.querySelector('.iconsWrap');
    const ringSilenceMenuFooter = document.querySelector('.ringSilenceMenuFooter');
    const notyfyWrap = document.querySelector('.notyfyWrap');
    
    function clickMissPerson(e){
        if(e.target !== person
            && person.contains(e.target) !== true) {
                topSilenceMenu.classList.add('close');
                person.classList.remove('personOpen');
                document.removeEventListener('click', clickMissPerson)
        };
    };

    function clickMissRing(e){        
        if(e.target !== ringIcon
            && ringIcon.contains(e.target) !== true) {
                ringSilenceMenu.classList.add('close');
                ringSilenceMenuFooter.classList.remove('close');
                notyfyWrap.classList.remove('maxWrap');
                ringIcon.classList.remove('openRingMenu');
                document.removeEventListener('click', clickMissRing);
        };
    };

    function clickMissNews(e){
        if(e.target !== newsPagItemIconPlus
            && newsSilenceMenu.contains(e.target) !== true) {
                newsSilenceMenu.classList.add('close');
                document.removeEventListener('click', clickMissNews);
        };
    };

    function clickMissWhatsNewBlock(e){
        if(e.target !== whatsNewBlock
            && whatsNewBlock.contains(e.target) !== true) {
                whatsNewBlock.classList.remove('whatsNewMax');
                iconWrap.classList.add('iconsWrapSmallPosition');
                document.removeEventListener('click', clickMissWhatsNewBlock);
        };
    };

    function clickMissMusic(e){
        if(e.target !== musicIcon
        && musicIcon.contains(e.target) !== true
        && e.target !== silenceWalkman) {
            musicSilenceMenu.classList.add('close');
            musicIcon.classList.remove('openMusicMenu');
            document.removeEventListener('click', clickMissMusic);
        };
    };

    person.addEventListener('click', function(){
        topSilenceMenu.classList.toggle('close');
        person.classList.toggle('personOpen');
        document.addEventListener('click', clickMissPerson)
    });

    inputInContainer1.addEventListener('focus', function(){
        inputInContainer1.classList.add('focus');
        searchSilenceMenu.classList.remove('close');
    });
    inputInContainer1.addEventListener('blur', function(){
        inputInContainer1.classList.remove('focus');
        searchSilenceMenu.classList.add('close');
    });

    ringIcon.addEventListener('click', function(e){
        if(ringSilenceMenu.contains(e.target)){
            return
        };
        ringSilenceMenu.classList.toggle('close');
        ringIcon.classList.toggle('openRingMenu');
        document.addEventListener('click', clickMissRing);
    });

    musicIcon.addEventListener('click', function(){
            musicSilenceMenu.classList.toggle('close');
            musicIcon.classList.toggle('openMusicMenu');
            document.addEventListener('click', clickMissMusic);
    });

    document.querySelector('.walkman').addEventListener('click', function(){
        silenceWalkman.classList.remove('close');
    });
    silenceWalkman.addEventListener('click', function(){
        musicSilenceMenu.classList.toggle('close');
        document.addEventListener('click', clickMissMusic);
    });
    document.querySelector('.toogleWrap').addEventListener('click',function(){
        switchBtn.classList.toggle('switch-on');
    });

    for (let i = 0; i < newsPag.length; i++) {
        newsPag[i].addEventListener('click', function(e){
            const newsPagItemActive = document.querySelector('.newsPag.active');
            newsPagItemActive.classList.remove('active');
            e.currentTarget.classList.add('active');
            if(e.currentTarget !== newsPag[0]){
                silenceBlock.classList.add('close');
            };
        });
    };

    newsPag[0].addEventListener('click', function(){
        silenceBlock.classList.remove('close')
    });
    newsPagItemIconPlus.addEventListener('click',function(event){
        event.stopPropagation();
        newsSilenceMenu.classList.toggle('close');
        document.addEventListener('click', clickMissNews);
    });

    for (let i = 0; i < silenceNewsItem.length; i++) {
        silenceNewsItem[i].addEventListener('click', function(event){
            event.stopPropagation();
            var a = '[data-name='+ langId[silenceNewsItem[i].children[1].innerText] + ']';
            document.querySelectorAll(a).forEach(function(el){
                el.classList.toggle('close');
            });
        });
    };
    document.querySelector('.hideButton').addEventListener('click', function(){
        document.querySelector('.recomends').remove();
    });

    var cur = 0;
    arrowLeft.addEventListener('click', function(){
        if(cur+460 < 0){
            cur+=460 ;
            moveTo(cur)
        } else {
            cur = 0;
            moveTo(cur)
        };
    });
    arrowRight.addEventListener('click', function(){ 
        if(cur-460>(previewChildren.length*-100)+520){
            cur-=460;
            moveTo(cur)
        } else {
            cur = (previewChildren.length*-100)+520
            moveTo(cur)
        };
    });
    function moveTo(direction){
        preview.style.transform = "translateX("+ direction + "px)";
        if (cur === 0){
            arrowLeft.classList.add('close');
        } else{
            arrowLeft.classList.remove('close');
        }
        if (cur === (previewChildren.length*-100)+520){
            arrowRight.classList.add('close');
        } else {
            arrowRight.classList.remove('close');
        };
    };

    whatsNewBlock.querySelector('input').addEventListener('click', function(){
        whatsNewBlock.classList.add('whatsNewMax');
        iconWrap.classList.remove('iconsWrapSmallPosition');
        document.addEventListener('click', clickMissWhatsNewBlock);
    });


    for (let i = 0; i < document.querySelectorAll('.likeCount').length; i++) {
        document.querySelectorAll('.likeCount')[i].innerHTML = localStorage.getItem('like' + i);
    };
    for (let i = 0; i < document.querySelectorAll('.commentCount').length; i++) {
        document.querySelectorAll('.commentCount')[i].innerHTML = localStorage.getItem('comment' + i);
    };
    for (let i = 0; i < document.querySelectorAll('.repostCount').length; i++) {
        document.querySelectorAll('.repostCount')[i].innerHTML = localStorage.getItem('repost' + i);
    };



    for (let i = 0; i < document.querySelectorAll('.likeIcon').length; i++) {
        document.querySelectorAll('.likeIcon')[i].addEventListener('click', function(){ Count('.likeCount', 'like'+ i, i)});
    };
    for (let i = 0; i < document.querySelectorAll('.commentIcon').length; i++) {
        document.querySelectorAll('.commentIcon')[i].addEventListener('click', function(){ Count('.commentCount', 'comment' + i, i)});
    };
    for (let i = 0; i < document.querySelectorAll('.repostIcon').length; i++) {
        document.querySelectorAll('.repostIcon')[i].addEventListener('click', function(){ Count('.repostCount', 'repost'+ i, i)});
    };

    function Count(element, nameEL, index){
        var count = Number(localStorage.getItem(nameEL));
        localStorage.setItem(nameEL, count+1 );
        document.querySelectorAll(element)[index].innerHTML = localStorage.getItem(nameEL);
    };

    const notify = document.querySelectorAll('.notify')[1];
    ringSilenceMenuFooter.addEventListener('click',function(){
        ringSilenceMenuFooter.classList.add('close');
        notyfyWrap.classList.add('maxWrap');
        for (let i = 0; i < 3; i++) {
            notyfyWrap.appendChild(notify.cloneNode(true));
        };
    });

});